package io.art.tests.L3.core;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class Base {

    @BeforeSuite
    public void setUp() {
        Configuration.baseUrl = "https://the-internet.herokuapp.com";
    }
}
